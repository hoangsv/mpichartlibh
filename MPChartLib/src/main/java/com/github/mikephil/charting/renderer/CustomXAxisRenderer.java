package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.Color;

import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.Collections;
import java.util.List;

public class CustomXAxisRenderer extends XAxisRenderer {

  List<String> labelList;
  List<Boolean> haveCommentList;

  public CustomXAxisRenderer(ViewPortHandler viewPortHandler, XAxis xAxis, Transformer trans) {
    super(viewPortHandler, xAxis, trans);
    labelList = Collections.EMPTY_LIST;
    haveCommentList = Collections.EMPTY_LIST;
  }

  public CustomXAxisRenderer(ViewPortHandler viewPortHandler, XAxis xAxis, Transformer trans, List<String> labelList, List<Boolean> haveCommentList) {
    super(viewPortHandler, xAxis, trans);
    this.labelList = labelList;
    this.haveCommentList = haveCommentList;
  }

  @Override
  protected void drawLabels(Canvas c, float pos, MPPointF anchor) {
    final float labelRotationAngleDegrees = mXAxis.getLabelRotationAngle();
    boolean centeringEnabled = mXAxis.isCenterAxisLabelsEnabled();

    float[] positions = new float[mXAxis.mEntryCount * 2];

    for (int i = 0; i < positions.length; i += 2) {

      // only fill x values
      if (centeringEnabled) {
        positions[i] = mXAxis.mCenteredEntries[i / 2];
      } else {
        positions[i] = mXAxis.mEntries[i / 2];
      }
    }

    mTrans.pointValuesToPixel(positions);

    for (int i = 0; i < positions.length; i += 2) {

      float x = positions[i];

      if (mViewPortHandler.isInBoundsX(x)) {

        String label = mXAxis.getValueFormatter().getFullValue(mXAxis.mEntries[i / 2], mXAxis);
        String labelCouldShort = mXAxis.getValueFormatter().getFormattedValue(mXAxis.mEntries[i / 2], mXAxis);
        int color;
        if (labelList.contains(label)) {
          if (labelList.indexOf(label) < haveCommentList.size()) {
            if (haveCommentList.get(labelList.indexOf(label))) {
              color = Color.BLUE;
            } else {
              color = mXAxis.getTextColor();
            }
          } else {
            color = mXAxis.getTextColor();
          }
        } else {
          color = mXAxis.getTextColor();
        }

        mAxisLabelPaint.setColor(color);

        if (mXAxis.isAvoidFirstLastClippingEnabled()) {

          // avoid clipping of the last
          if (i == mXAxis.mEntryCount - 1 && mXAxis.mEntryCount > 1) {
            float width = Utils.calcTextWidth(mAxisLabelPaint, labelCouldShort);

            if (width > mViewPortHandler.offsetRight() * 2
                && x + width > mViewPortHandler.getChartWidth())
              x -= width / 2;

            // avoid clipping of the first
          } else if (i == 0) {

            float width = Utils.calcTextWidth(mAxisLabelPaint, labelCouldShort);
            x += width / 2;
          }
        }

        drawLabel(c, labelCouldShort, x, pos, anchor, labelRotationAngleDegrees);
      }
    }
  }
}